#!/usr/bin/python
from argparse import ArgumentParser
from PIL import Image
import os
Image.MAX_IMAGE_PIXELS = 400000000

parser = ArgumentParser()
parser.add_argument("-f", "--file", dest="filename",
                    help="Image file name")
parser.add_argument("-x1", "--left", dest="x1",
                    help="Left x value", type=int)
parser.add_argument("-x2", "--right", dest="x2",
                    help="Right x value", type=int)
parser.add_argument("-y1", "--top", dest="y1",
                    help="Top y value", type=int)
parser.add_argument("-y2", "--bottom", dest="y2",
                    help="Bottom y value", type=int)
parser.add_argument("-p", "--path", dest="path",
                    help="Base directory path for image files")
parser.add_argument("-q", "--quality", dest="quality",
                    help="JPG output quality", type=int)
args = parser.parse_args()

imageFile = os.path.join(args.path, args.filename)
croppedImageFile = os.path.join(args.path, args.filename.replace('tif','jpg'))
print("[INFO] Cropping image: " + imageFile)
print("[INFO] Cropped Image File = " + croppedImageFile)
img = Image.open(imageFile)
area=(args.x1,args.y1,args.x2,args.y2)
croppedImage = img.crop(area)
convertedImage = croppedImage.convert('L')
img = None
croppedImage = None
convertedImage.save(croppedImageFile, 'JPEG', quality=args.quality)
