#!/bin/bash

DIR=$1
QUALITY=$2

#for filename in $DIR/*.tif; do
#    ./preprocess.py --path=$DIR --file=$filename -x1 500 -y1 500 -x2 12000 -y2 18000 --quality=$QUALITY
#done

#./find_contiguous.pl $DIR;
tar czvf $DIR/preprocessed.tar.gz $DIR/*.jpg
BUCKET=s3://hamilton-1961-$QUALITY
echo $BUCKET
aws s3 mb $BUCKET
aws s3 cp $DIR/preprocessed.tar.gz $BUCKET
