#!/usr/bin/python

from argparse import ArgumentParser
from PIL import Image
import os
import cv2
import glob
import imutils

parser = ArgumentParser()
parser.add_argument("-p", "--path", dest="imageDir",
                    help="Image directory name")
# loop over the image paths, load each one, and add them to our
# images to stitch list
args = parser.parse_args()
imageDir = args.imageDir
print(imageDir)
outputFile = os.path.join(imageDir, "out.tif")
print(outputFile)
images = []

for imageFile in glob.glob(imageDir + '/*'):
    image = cv2.imread(imageFile)
    print(imageFile)
    images.append(image)

# initialize OpenCV's image stitcher object and then perform the image
# stitching
print("[INFO] stitching images...")
# stitcher = cv2.createStitcher() if imutils.is_cv3() else cv2.Stitcher_create()
stitcher = cv2.Stitcher.create(mode=1)
(status, stitched) = stitcher.stitch(images)

# if the status is '0', then OpenCV successfully performed image
# stitching
if status == 0:
    # write the output stitched image to disk
    print("[INFO] Writing to disk")
    cv2.imwrite(outputFile, stitched)

# otherwise the stitching failed, likely due to not enough keypoints)
# being detected
else:
    print("[INFO] image stitching failed ({})".format(status))

images = None
stitcher = None
stitched = None
