#!/usr/bin/perl
use Data::Dumper;
use Number::Range;
use List::Util qw( min max );
use File::Copy;

my $dir = $ARGV[0];
my @files = <"${dir}/*.jpg">;
my @fileNumbers;

foreach (@files) {
  my ($fileNumber) = /-(\d\d\d\d\d\d)/;
  push @fileNumbers, $fileNumber;
}

my $range = Number::Range->new();
$range->addrange(@fileNumbers);
my @rangeList = $range->rangeList;
my $rangeListString = join ',', map join('-',@$_), $range->rangeList;

for my $rangeDir (split(/,/, $rangeListString)){
    mkdir("${dir}/${rangeDir}");
}

for my $thisRange (@rangeList) {
    my $newDirName = '';
    my $start = min(@{$thisRange});
    my $end = max(@{$thisRange}); 
    if ($start == $end) { 
        $newDirName = $start;     
    } else {
        $newDirName = "${start}-${end}";
    } 
    foreach (@files) {
        my ($fileNumber) = /-(\d\d\d\d\d\d)/;
        if ($start <= $fileNumber && $fileNumber <= $end){
            my $fromFile = $_;
            my $toFile = "${dir}/${newDirName}/${fileNumber}.jpg";
            print("$fromFile\n");
            print("$toFile\n");
            move($fromFile, $toFile);
        }
    }
    system("./stitch.py", "--path=${dir}/${newDirName}/")
}
